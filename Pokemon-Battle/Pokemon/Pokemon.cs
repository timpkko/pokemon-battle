﻿using System;

namespace Pokemon_Battle
{
    [Serializable]
    public class Pokemon
    {
        public string id { get; }
        public string name { get; }
        public PokemonTypes type { get; }
        public int HP { get; }
        public int ATK { get; }
        public int DEF { get; }
        public int SPD { get; }
        public string evo { get; }

        public Pokemon(string id, string name, PokemonTypes type, int HP, int ATK, int DEF, int SPD, string evo)
        {
            if (HP <= 0 || ATK <= 0 || DEF <= 0 || SPD <= 0)
            {
                throw new System.ArgumentException("Invalid stat");
            }

            this.id = id;
            this.name = name;
            this.type = type;
            this.HP = HP;
            this.ATK = ATK;
            this.DEF = DEF;
            this.SPD = SPD;
            this.evo = evo;
        }

        public override string ToString()
        {
            return "Name: " + name +
                   "\nType: " + type +
                   "\nHP: " + HP +
                   "\nATK: " + ATK +
                   "\nDEF: " + DEF +
                   "\nSPD: " + SPD;
        }
    }
}
