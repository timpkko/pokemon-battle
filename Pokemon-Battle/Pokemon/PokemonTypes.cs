﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Battle
{
    public enum PokemonTypes
    {
        Rock, Psychic, Normal, Fire, Water, Grass, Bug, Ground, Flying, Electric, Poison, Fighting, Ghost, Dragon, Ice
    }
}
