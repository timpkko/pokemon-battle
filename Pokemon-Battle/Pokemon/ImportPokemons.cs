﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Pokemon_Battle
{
    public class ImportPokemons
    {
        public static List<Pokemon> Import()
        {
            var pokemons = new List<Pokemon>();
            foreach(string line in File.ReadAllLines("Pokemon/Pokemons.txt"))
            {
                string[] splited = line.Split(',');
                if (splited[0].Equals("id"))
                    continue;

                pokemons.Add(new Pokemon(splited[0], splited[1], (PokemonTypes)Enum.Parse(typeof(PokemonTypes), splited[2]), int.Parse(splited[3]), int.Parse(splited[4]), int.Parse(splited[5]), int.Parse(splited[6]), splited[7]));
            }

            return pokemons;
        }
    }
}
