﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Pokemon_Battle.Pages
{
    public partial class StartScreen : UserControl
    {
        BitmapImage[] bgs { get; }

        public StartScreen()
        {
            InitializeComponent();
            bgs = new BitmapImage[5]
            {
                new BitmapImage(new Uri("../images/battle_backgrounds/beach_bg.png", UriKind.Relative)),
                new BitmapImage(new Uri("../images/battle_backgrounds/grass_bg.png", UriKind.Relative)),
                new BitmapImage(new Uri("../images/battle_backgrounds/rock_bg.png", UriKind.Relative)),
                new BitmapImage(new Uri("../images/battle_backgrounds/stadium_bg.png", UriKind.Relative)),
                new BitmapImage(new Uri("../images/battle_backgrounds/water_bg.png", UriKind.Relative))
            };
        }

        private void ListBoxBG_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MessageBG.Visibility = Visibility.Visible;
            ImageBG.Source = bgs[ListBoxBG.SelectedIndex];
        }

        private void ChooseBtn_Click(object sender, RoutedEventArgs e)
        {
            if (PlayerNameTxtBox.Text.Equals(""))
                MessageBox.Show("You must enter a name");
            else if (ListBoxBG.SelectedIndex == -1)
                MessageBox.Show("You must select a background");
            else
                Switcher.Switch(new PokemonSelection(this));
        }
    }
}
