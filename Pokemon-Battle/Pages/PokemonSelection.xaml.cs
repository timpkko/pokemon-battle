﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Media;

namespace Pokemon_Battle.Pages
{
    /// <summary>
    /// Interaction logic for PokemonSelection.xaml
    /// </summary>
    public partial class PokemonSelection : UserControl
    {
        StartScreen sc;
        List<Pokemon> pokemonList = ImportPokemons.Import();
        public List<Pokemon> SelectedPokemons { get; }

        public PokemonSelection(StartScreen sc)
        {
            InitializeComponent();

            this.sc = sc;
            foreach (Pokemon poke in pokemonList)
            {
                StackPanel stackPanel = new StackPanel();
                ListBoxItem listItem = new ListBoxItem();

                Image img = new Image
                {
                    Source = new BitmapImage(new Uri("../images/pokemon_opponent/" + poke.evo + "/" + poke.id + ".png", UriKind.Relative)),
                    Stretch = Stretch.Uniform
                };

                RenderOptions.SetBitmapScalingMode(img, BitmapScalingMode.HighQuality);

                Label pokemonName = new Label()
                {
                    Content = poke.name,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "fonts/#Pokemon Pixel Font"),
                    FontSize = 20
                };

                stackPanel.Children.Add(img);
                stackPanel.Children.Add(pokemonName);
                PokemonBox.Items.Add(stackPanel);
            }

            SelectedPokemons = new List<Pokemon>();
        }
        private void PokemonBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Pokemon pokemon = pokemonList[PokemonBox.SelectedIndex];

            PokemonImg.Source = new BitmapImage(new Uri("../images/artwork/" + pokemon.id + ".jpg", UriKind.Relative));
            NameLabel.Content = pokemon.name;
            TypeImg.Source = new BitmapImage(new Uri("../images/sprites/types/" + pokemon.type + ".png", UriKind.Relative));
            HPLbl.Content = pokemon.HP;
            ATKLbl.Content = pokemon.ATK;
            DEFLbl.Content = pokemon.DEF;
            SPDLbl.Content = pokemon.SPD;
        }

        private void ChoosePokemonBtn_Click(object sender, RoutedEventArgs e)
        {
            if (PokemonBox.SelectedIndex <= -1)
                ErrorLabel.Content = "Select one Pokemon";
            else if (SelectedPokemons.Contains(pokemonList[PokemonBox.SelectedIndex]))
                ErrorLabel.Content = "Duplicates are not allowed";
            else
            {
                Pokemon poke = pokemonList[PokemonBox.SelectedIndex];
                SelectedPokemons.Add(poke);
                YourPokemonBox.Items.Add(new Image()
                {
                    Source = new BitmapImage(new Uri("../images/pokemon_opponent/" + poke.evo + "/" + poke.id + ".png", UriKind.Relative)),
                    Stretch = Stretch.None
                });
                ErrorLabel.Content = "";
            }

            if (SelectedPokemons.Count >= 6)
            {
                FinishBtn.IsEnabled = true;
                ChoosePokemonBtn.IsEnabled = false;
            }
            else if (SelectedPokemons.Count > 0)
            {
                FinishBtn.IsEnabled = true;
                ChoosePokemonBtn.IsEnabled = true;
            }
            else
            {
                FinishBtn.IsEnabled = false;
                ChoosePokemonBtn.IsEnabled = true;
            }
        }

        private void FinishBtn_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new BattleScene(sc, SelectedPokemons));
        }

        private void RemoveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (YourPokemonBox.SelectedIndex <= -1)
            {
                try
                {
                    SelectedPokemons.Remove(SelectedPokemons[SelectedPokemons.Count - 1]);
                    YourPokemonBox.Items.Remove(YourPokemonBox.Items[YourPokemonBox.Items.Count - 1]);
                }
                catch { }
            }
            else
            {
                SelectedPokemons.RemoveAt(YourPokemonBox.SelectedIndex);
                YourPokemonBox.Items.Remove(YourPokemonBox.SelectedItem);
            }

            if (SelectedPokemons.Count >= 6)
            {
                FinishBtn.IsEnabled = true;
                ChoosePokemonBtn.IsEnabled = false;
            }
            else if (SelectedPokemons.Count > 0)
            {
                FinishBtn.IsEnabled = true;
                ChoosePokemonBtn.IsEnabled = true;
            }
            else
            {
                FinishBtn.IsEnabled = false;
                ChoosePokemonBtn.IsEnabled = true;
            }
        }

        private void PokemonBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (SelectedPokemons.Count >= 6)
                e.Handled = true;
            else
            {
                e.Handled = false;
                ChoosePokemonBtn_Click(sender, e);
            }

        }
    }
}
