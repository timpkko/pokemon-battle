﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Win32;
using System.IO;
using Pokemon_Battle.Battle;

namespace Pokemon_Battle.Pages
{
    /// <summary>
    /// Interaction logic for BattleScene.xaml
    /// </summary>
    public partial class BattleScene : UserControl
    {
        List<Pokemon> pokeList;
        List<Pokemon> opponentPokemons;
        Pokemon currentPokemon;
        Pokemon currentOpponentPokemon;
        DispatcherTimer timer;
        int seconds = 60;

        public BattleScene(StartScreen sc, List<Pokemon> pokeList)
        {
            InitializeComponent();
            this.pokeList = pokeList;
            BgImg.Source = sc.ImageBG.Source;
            currentPokemon = pokeList[0];
            UserPokemon.Source = new BitmapImage(new Uri("../images/pokemon_user/" + pokeList[0].evo + "/" + pokeList[0].id + ".png", UriKind.Relative));
            Move(UserPokemon, 0, -50, 0.3);
            PokemonNameLbl.Content = pokeList[0].name.ToUpper();
            CurrentHPLbl.Content = pokeList[0].HP;
            PokemonHPLbl.Content = pokeList[0].HP;
            
            AddPokemonsToListBox();
            setTimer();
            timer.Start();

            opponentPokemons = new List<Pokemon>();
            var pokesds = ImportPokemons.Import();
            Random rand = new Random();
            for (int i = 0; i < 6; i++)
            {
                opponentPokemons.Add(pokesds[rand.Next(25)]);
            }
            currentOpponentPokemon = opponentPokemons[0];
            Move(ActionsImg, 0, -200, 0.5);
            EnemyPokemon.Source = new BitmapImage(new Uri("../images/pokemon_opponent/" + opponentPokemons[0].evo + "/" + opponentPokemons[0].id + ".png", UriKind.Relative));
        }

        private void Move(UIElement elememt, double newX, double newY, double seconds)
        {
            Vector offset = VisualTreeHelper.GetOffset(elememt);
            var top = offset.Y;
            var left = offset.X;
            TranslateTransform trans = new TranslateTransform();
            elememt.RenderTransform = trans;
            DoubleAnimation anim1 = new DoubleAnimation(top, newY - top, TimeSpan.FromSeconds(seconds));
            DoubleAnimation anim2 = new DoubleAnimation(left, newX - left, TimeSpan.FromSeconds(seconds));
            trans.BeginAnimation(TranslateTransform.XProperty, anim2);
            trans.BeginAnimation(TranslateTransform.YProperty, anim1);
        }

        private void MoveRight(UIElement elememt, double newX, double seconds)
        {
            Vector offset = VisualTreeHelper.GetOffset(elememt);
            var left = offset.X;
            TranslateTransform trans = new TranslateTransform();
            elememt.RenderTransform = trans;
            DoubleAnimation anim2 = new DoubleAnimation(newX - left, left, TimeSpan.FromSeconds(seconds));
            trans.BeginAnimation(TranslateTransform.XProperty, anim2);
        }

        private void MoveLeft(UIElement elememt, double newX, double seconds)
        {
            Vector offset = VisualTreeHelper.GetOffset(elememt);
            var left = offset.X;
            TranslateTransform trans = new TranslateTransform();
            elememt.RenderTransform = trans;
            DoubleAnimation anim2 = new DoubleAnimation(left, newX + left, TimeSpan.FromSeconds(seconds));
            trans.BeginAnimation(TranslateTransform.XProperty, anim2);
        }

        private void MoveUp(UIElement elememt, double newY, double seconds)
        {
            Vector offset = VisualTreeHelper.GetOffset(elememt);
            var top = offset.Y;
            TranslateTransform trans = new TranslateTransform();
            elememt.RenderTransform = trans;
            DoubleAnimation anim2 = new DoubleAnimation(top, newY - top, TimeSpan.FromSeconds(seconds));
            trans.BeginAnimation(TranslateTransform.XProperty, anim2);
        }

        private void FightBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            CursorImg.Visibility = Visibility.Visible;
        }

        private void FightBtn_MouseLeave(object sender, MouseEventArgs e)
        {
            CursorImg.Visibility = Visibility.Hidden;
        }

        private void PokemonBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor2.Visibility = Visibility.Visible;
        }

        private void PokemonBtn_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor2.Visibility = Visibility.Hidden;
        }

        private void PokemonBtn_Click(object sender, RoutedEventArgs e)
        {
            Move(YourPokemonsGrid, 0, 467, 0.3);
        }

        private void FightBtn_Click(object sender, RoutedEventArgs e)
        {
           // Battle.battle.Damage(currentPokemon, currentOpponentPokemon, move, true);
        }

        private void AddPokemonsToListBox()
        {
            foreach (Pokemon poke in pokeList)
            {
                YourPokemonsListbox.Items.Add(new Image()
                {
                    Source = new BitmapImage(new Uri("../images/pokemon_opponent/" + poke.evo + "/" + poke.id + ".png", UriKind.Relative)),
                    Stretch = Stretch.None
                });
            }
        }

        private void GoBackBtn_Click(object sender, RoutedEventArgs e)
        {
            MoveRight(YourPokemonsGrid, 0, 0.5);
        }

        private void GoBackBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            CursorBack.Visibility = Visibility.Visible;
        }

        private void GoBackBtn_MouseLeave(object sender, MouseEventArgs e)
        {
            CursorBack.Visibility = Visibility.Hidden;
        }

        private void YourPokemonsListbox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            currentPokemon = pokeList[YourPokemonsListbox.SelectedIndex];
            UserPokemon.Source = new BitmapImage(new Uri("../images/pokemon_user/" + currentPokemon.evo + "/" + currentPokemon.id + ".png", UriKind.Relative));
            PokemonNameLbl.Content = currentPokemon.name.ToUpper();
            CurrentHPLbl.Content = currentPokemon.HP;
            PokemonHPLbl.Content = currentPokemon.HP;
            MoveRight(YourPokemonsGrid, 0, 0.5);
        }

        private void setTimer()
        {
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            TimerLbl.Content = string.Format("{0:mm\\:ss}", (TimeSpan.FromSeconds(seconds)));
            seconds--;
            if (seconds == 0)
                timer.Stop();
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            timer.Stop();
            SaveFileDialog saveFD = new SaveFileDialog
            {
                Title = "Save state file",
                Filter = "State file|*.state"
            };

            if (saveFD.ShowDialog() == true)
            {
                BinaryFormatter formatter = new BinaryFormatter();
                using (FileStream file = new FileStream(saveFD.FileName, FileMode.Create))
                {
                    State state = new State(pokeList, opponentPokemons, currentPokemon, currentOpponentPokemon, seconds);
                    formatter.Serialize(file, state);
                }
            }
            timer.Start();
        }

        private void SaveBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor3.Visibility = Visibility.Visible;
        }

        private void SaveBtn_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor3.Visibility = Visibility.Hidden;
        }

        private void LoadBtn_Click(object sender, RoutedEventArgs e)
        {

            OpenFileDialog ofd = new OpenFileDialog
            {
                Title = "Open state file",
                Filter = "State file|*.state"
            };

            if (ofd.ShowDialog() == true)
            {
                State state = null;
                BinaryFormatter formatter = new BinaryFormatter();
                using (FileStream reader = new FileStream(ofd.FileName, FileMode.Open))
                {
                    state = (State)formatter.Deserialize(reader);
                }
                YourPokemonsListbox.Items.Clear();
                pokeList = state.yourPokemons;
                opponentPokemons = state.opponentPokemons;
                currentPokemon = state.currentPokemon;
                currentOpponentPokemon = state.oppoentCurrentPokemon;
                seconds = state.timer;

                AddPokemonsToListBox();

                UserPokemon.Source = new BitmapImage(new Uri("../images/pokemon_user/" + currentPokemon.evo + "/" + currentPokemon.id + ".png", UriKind.Relative));
                EnemyPokemon.Source = new BitmapImage(new Uri("../images/pokemon_opponent/" + currentOpponentPokemon.evo + "/" + currentOpponentPokemon.id + ".png", UriKind.Relative));
            }
        }

        private void LoadBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor4.Visibility = Visibility.Visible;
        }

        private void LoadBtn_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor4.Visibility = Visibility.Hidden;
        }
    }
}
