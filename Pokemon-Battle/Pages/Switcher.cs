﻿using System.Windows.Controls;

// example from https://azerdark.wordpress.com/2010/04/23/multi-page-application-in-wpf/

namespace Pokemon_Battle.Pages
{
    public static class Switcher
    {
        public static MainWindow pageSwitcher;

        public static void Switch(UserControl newPage)
        {
            pageSwitcher.Navigate(newPage);
        }
    }
}
