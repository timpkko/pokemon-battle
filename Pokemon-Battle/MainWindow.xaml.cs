﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Pokemon_Battle.Pages;

namespace Pokemon_Battle
{
    // example from https://azerdark.wordpress.com/2010/04/23/multi-page-application-in-wpf/

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Switcher.pageSwitcher = this;
            Switcher.Switch(new StartScreen());
        }

        public void Navigate(UserControl nextPage)
        {
            Content = nextPage;
        }
    }
}
