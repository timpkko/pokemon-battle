﻿using Pokemon_Battle;
using System.Collections.Generic;

public class Moves
{
    public string name { get; }
    public int Power { get; }
    public int pp { get; }
    public PokemonTypes type { get; }

    public Moves ( string name, int power, int pp, PokemonTypes type)
    {
        if ( power <= 0 || pp <= 0 )
        {
            throw new System.ArgumentException("Invalid Move Stats");
        }

        this.name = name;
        this.Power = power;
        this.pp = pp;
        this.type = type;
    }

    public static List<Moves> MoveSet(string name)
    {
        if (name.Equals("Cubone"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Bone Club", 65, 20, PokemonTypes.Ground),
                new Moves("Headbutt", 90, 15, PokemonTypes.Water),
                new Moves("Flamethrower", 90, 15, PokemonTypes.Fire),
                new Moves("Aerial Ace", 60, 15, PokemonTypes.Flying)
            };
            return MoveSet;
        }

        if (name.Equals("Lapras"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Body Slam", 85, 15, PokemonTypes.Normal),
                new Moves("Surf", 100, 15, PokemonTypes.Water),
                new Moves("Ice Beam", 90, 15, PokemonTypes.Ice),
                new Moves("Thunderbolt", 60, 15, PokemonTypes.Electric)
            };
            return MoveSet;
        }

        if (name.Equals("Abra"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Shadow Ball", 80, 15, PokemonTypes.Ghost),
                new Moves("Psyshock", 80, 10, PokemonTypes.Psychic),
                new Moves("Ice Punch", 75, 15, PokemonTypes.Ice),
                new Moves("Fire Punch", 75, 15, PokemonTypes.Fire)
            };
            return MoveSet;
        }

        if (name.Equals("Pikachu"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Thunderbolt", 90, 15, PokemonTypes.Electric),
                new Moves("Slam", 80, 15, PokemonTypes.Normal),
                new Moves("Thunder Shock", 40, 30, PokemonTypes.Electric),
                new Moves("Brick Break", 75, 15, PokemonTypes.Fighting)
            };
            return MoveSet;
        }

        if (name.Equals("Snorlax"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Body Slam", 85, 15, PokemonTypes.Normal),
                new Moves("Earthquake", 100, 10, PokemonTypes.Ground),
                new Moves("Fire Blast", 110, 5, PokemonTypes.Fire),
                new Moves("Psychic", 90, 10, PokemonTypes.Psychic)
            };
            return MoveSet;
        }

        if (name.Equals("Blastoise"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Hydro Pump", 110, 5, PokemonTypes.Water),
                new Moves("Ice Beam", 90, 10, PokemonTypes.Ice),
                new Moves("Earthquake", 100, 10, PokemonTypes.Ground),
                new Moves("Surf", 90, 15, PokemonTypes.Water)
            };
            return MoveSet;
        }

        if (name.Equals("Dugtrio"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Earthquake", 110, 10, PokemonTypes.Ground),
                new Moves("Slash", 70, 20, PokemonTypes.Normal),
                new Moves("Aerial", 60, 20, PokemonTypes.Flying),
                new Moves("Ancient Power", 60, 5, PokemonTypes.Rock)
            };
            return MoveSet;
        }

        if (name.Equals("Ninetales"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Flamethrower", 90, 15, PokemonTypes.Fire),
                new Moves("Ice Beam", 90, 10, PokemonTypes.Ice),
                new Moves("Extrasensory", 80, 20, PokemonTypes.Psychic),
                new Moves("Energy Ball", 90, 10, PokemonTypes.Grass)
            };
            return MoveSet;
        }

        if (name.Equals("Muk"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Gunk Shot", 120, 5, PokemonTypes.Poison),
                new Moves("Sludge Wave", 95, 10, PokemonTypes.Poison),
                new Moves("Shadow Ball", 80, 15, PokemonTypes.Ghost),
                new Moves("Stone Edge", 100, 5, PokemonTypes.Rock)
            };
            return MoveSet;
        }

        if (name.Equals("Gengar"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Energy Ball", 90, 10, PokemonTypes.Grass),
                new Moves("Thunderbolt", 90, 15, PokemonTypes.Electric),
                new Moves("Shadow Ball", 80, 15, PokemonTypes.Ghost),
                new Moves("Fire Punch", 75, 15, PokemonTypes.Fire)
            };
            return MoveSet;
        }

        if (name.Equals("Hitmonlee"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Close Combat", 120, 5, PokemonTypes.Fighting),
                new Moves("Brick Break", 75, 15, PokemonTypes.Fighting),
                new Moves("Blaze Kick", 85, 10, PokemonTypes.Fire),
                new Moves("Stone Edge", 100, 5, PokemonTypes.Rock)
            };
            return MoveSet;
        }

        if (name.Equals("Pinsir"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Close Combat", 120, 5, PokemonTypes.Fighting),
                new Moves("X-Scissor", 80, 15, PokemonTypes.Bug),
                new Moves("Earthquake", 100, 10, PokemonTypes.Ground),
                new Moves("Stone Edge", 100, 5, PokemonTypes.Rock)
            };
            return MoveSet;
        }

        if (name.Equals("Dragonair"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Aqua Tail", 90, 10, PokemonTypes.Water),
                new Moves("Dragon Pulse", 85, 10, PokemonTypes.Dragon),
                new Moves("Flamethrower", 90, 15, PokemonTypes.Fire),
                new Moves("Thunderbolt", 90, 15, PokemonTypes.Electric)
            };
            return MoveSet;
        }

        if (name.Equals("Magmar"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Karate Chop", 50, 25, PokemonTypes.Fighting),
                new Moves("Thunder Punch", 75, 15, PokemonTypes.Electric),
                new Moves("Flamethrower", 90, 15, PokemonTypes.Fire),
                new Moves("Fire Blast", 110, 5, PokemonTypes.Fire)
            };
            return MoveSet;
        }

        if (name.Equals("Tauros"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Zen Headbutt", 80, 15, PokemonTypes.Psychic),
                new Moves("Stone Edge", 100, 5, PokemonTypes.Rock),
                new Moves("Earthquake", 100, 10, PokemonTypes.Ground),
                new Moves("Horn Attack", 65, 25, PokemonTypes.Normal)
            };
            return MoveSet;
        }

        if (name.Equals("Arcanine"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Bulldoze", 60, 20, PokemonTypes.Ground),
                new Moves("Fire Blast", 110, 5, PokemonTypes.Fire),
                new Moves("Fire Fang", 65, 15, PokemonTypes.Fire),
                new Moves("Thunder Fang", 65, 15, PokemonTypes.Electric)
            };
            return MoveSet;
        }

        if (name.Equals("Venusaur"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Vine Whip", 45, 25, PokemonTypes.Grass),
                new Moves("Sludge Bomb", 90, 10, PokemonTypes.Poison),
                new Moves("Energy Ball", 90, 10, PokemonTypes.Grass),
                new Moves("Earthquake", 100, 10, PokemonTypes.Ground)
            };
            return MoveSet;
        }

        if (name.Equals("Pidgeot"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Hurricane", 110, 10, PokemonTypes.Flying),
                new Moves("Air Slash", 75, 15, PokemonTypes.Flying),
                new Moves("Heatwave", 95, 10, PokemonTypes.Fire),
                new Moves("Tackle", 40, 35, PokemonTypes.Normal)
            };
            return MoveSet;
        }

        if (name.Equals("Golem"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Earthquake", 100, 10, PokemonTypes.Ground),
                new Moves("Stone Edge", 100, 5, PokemonTypes.Rock),
                new Moves("Tackle", 40, 35, PokemonTypes.Normal),
                new Moves("Thunderbolt", 90, 15, PokemonTypes.Electric)
            };
            return MoveSet;
        }

        if (name.Equals("Magneton"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Signal Beam", 75, 15, PokemonTypes.Bug),
                new Moves("Tri Attack", 80, 10, PokemonTypes.Normal),
                new Moves("Zap Cannon", 120, 5, PokemonTypes.Electric),
                new Moves("Thunderbolt", 90, 15, PokemonTypes.Electric)
            };
            return MoveSet;
        }

        if (name.Equals("Fearow"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Drill Peck", 80, 20, PokemonTypes.Flying),
                new Moves("Tri Attack", 80, 10, PokemonTypes.Normal),
                new Moves("Heatwave", 95, 10, PokemonTypes.Fire),
                new Moves("Drillrun", 80, 10, PokemonTypes.Ground)
            };
            return MoveSet;
        }

        if (name.Equals("Jigglypuff"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Thunderbolt", 90, 15, PokemonTypes.Electric),
                new Moves("Shadow Ball", 80, 15, PokemonTypes.Ghost),
                new Moves("Hyper Voice", 90, 10, PokemonTypes.Normal),
                new Moves("Wake-Up Slap", 70, 10, PokemonTypes.Fighting)
            };
            return MoveSet;
        }

        if (name.Equals("Nidoking"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Ice Beam", 90, 10, PokemonTypes.Ice),
                new Moves("Earthquake", 100, 10, PokemonTypes.Ground),
                new Moves("Sludge Wave", 95, 10, PokemonTypes.Poison),
                new Moves("Thunderbolt", 90, 15, PokemonTypes.Electric)
            };
            return MoveSet;
        }

        if (name.Equals("Machamp"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Earthquake", 100, 10, PokemonTypes.Ground),
                new Moves("Stone Edge", 100, 5, PokemonTypes.Rock),
                new Moves("Close Combat", 120, 5, PokemonTypes.Fighting),
                new Moves("Dynamic Punch", 100, 15, PokemonTypes.Fighting)
            };
            return MoveSet;
        }

        if (name.Equals("Starmie"))
        {
            List<Moves> MoveSet = new List<Moves>
            {
                new Moves("Hydro Pump", 110, 5, PokemonTypes.Water),
                new Moves("Surf", 90, 15, PokemonTypes.Water),
                new Moves("Psychic", 90, 10, PokemonTypes.Psychic),
                new Moves("Thunderbolt", 90, 15, PokemonTypes.Electric)
            };
            return MoveSet;
        }
        throw new System.ArgumentException("Pokemon not in the list");
    }
}
