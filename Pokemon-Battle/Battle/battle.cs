﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Battle.Battle
{
    class battle
    {
        /**
        private Moves move;
        private pokemon attacker;
        private pokemon receiver;
        */
        public static int Damage(Pokemon attacker, Pokemon target, Moves move, bool critical)
        {
            double Type = Effectiveness(move, target);
            double crit;
            if (critical == true)
            {
                crit = 1.5;
            }
            else
            {
                crit = 1;
            }

            double Damage = (22 * move.Power * (attacker.ATK / target.DEF / 50 + 2) * (Type * crit));
            return (int)Damage;
        }

        public static bool Critical()
        {
            Random generator = new Random();
            int chance = generator.Next(1, 11);
            if (chance == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool AccuracyCheck(Moves move)
        {
            Random generator = new Random();
            if (move.Power <= 50)
            {
                int chance = generator.Next(1, 21);
                if (chance <= 19)
                {
                    return true;
                }
            }
            else if (move.Power > 50 && move.Power < 101)
            {
                int chance = generator.Next(1, 6);
                if (chance <= 4)
                {
                    return true;
                }
            }
            else if (move.Power > 100)
            {
                int chance = generator.Next(1, 21);
                if (chance <= 13)
                {
                    return true;
                }
            }
            return false;

        }

        public static int Fainted(List<Pokemon> enemyTeam, Pokemon userPokemon)
        {
            if (userPokemon.type == PokemonTypes.Fire)
            {
                for (int i = 0; i < enemyTeam.Count; i++)
                {
                    if (enemyTeam[i].type == PokemonTypes.Water || enemyTeam[i].type == PokemonTypes.Ground || enemyTeam[i].type == PokemonTypes.Rock)
                    {
                        return i;
                    }
                }

                for (int j = 0; j < enemyTeam.Count; j++)
                {
                    if (enemyTeam[j].type == PokemonTypes.Fire || enemyTeam[j].type == PokemonTypes.Dragon)
                    {
                        return j;
                    }
                }
            }

            if (userPokemon.type == PokemonTypes.Water)
            {
                for (int i = 0; i < enemyTeam.Count; i++)
                {
                    if (enemyTeam[i].type == PokemonTypes.Electric || enemyTeam[i].type == PokemonTypes.Grass)
                    {
                        return i;
                    }
                }

                for (int j = 0; j < enemyTeam.Count; j++)
                {
                    if (enemyTeam[j].type == PokemonTypes.Water || enemyTeam[j].type == PokemonTypes.Dragon)
                    {
                        return j;
                    }
                }
            }

            if (userPokemon.type == PokemonTypes.Grass)
            {
                for (int i = 0; i < enemyTeam.Count; i++)
                {
                    if (enemyTeam[i].type == PokemonTypes.Fire || enemyTeam[i].type == PokemonTypes.Ice || enemyTeam[i].type == PokemonTypes.Poison || enemyTeam[i].type == PokemonTypes.Flying || enemyTeam[i].type == PokemonTypes.Bug)
                    {
                        return i;
                    }
                }

                for (int j = 0; j < enemyTeam.Count; j++)
                {
                    if (enemyTeam[j].type == PokemonTypes.Grass || enemyTeam[j].type == PokemonTypes.Dragon)
                    {
                        return j;
                    }
                }
            }

            if (userPokemon.type == PokemonTypes.Normal)
            {
                for (int i = 0; i < enemyTeam.Count; i++)
                {
                    if (enemyTeam[i].type == PokemonTypes.Fighting || enemyTeam[i].type == PokemonTypes.Ghost)
                    {
                        return i;
                    }
                }
            }

            if (userPokemon.type == PokemonTypes.Rock)
            {
                for (int i = 0; i < enemyTeam.Count; i++)
                {
                    if (enemyTeam[i].type == PokemonTypes.Water || enemyTeam[i].type == PokemonTypes.Grass || enemyTeam[i].type == PokemonTypes.Fighting || enemyTeam[i].type == PokemonTypes.Ground)
                    {
                        return i;
                    }
                }
            }

            if (userPokemon.type == PokemonTypes.Electric)
            {
                for (int i = 0; i < enemyTeam.Count; i++)
                {
                    if (enemyTeam[i].type == PokemonTypes.Ground)
                    {
                        return i;
                    }
                }

                for (int j = 0; j < enemyTeam.Count; j++)
                {
                    if (enemyTeam[j].type == PokemonTypes.Electric || enemyTeam[j].type == PokemonTypes.Grass || enemyTeam[j].type == PokemonTypes.Dragon)
                    {
                        return j;
                    }
                }
            }

            if (userPokemon.type == PokemonTypes.Poison)
            {
                for (int i = 0; i < enemyTeam.Count; i++)
                {
                    if (enemyTeam[i].type == PokemonTypes.Ground || enemyTeam[i].type == PokemonTypes.Psychic)
                    {
                        return i;
                    }
                }

                for (int j = 0; j < enemyTeam.Count; j++)
                {
                    if (enemyTeam[j].type == PokemonTypes.Poison || enemyTeam[j].type == PokemonTypes.Rock || enemyTeam[j].type == PokemonTypes.Ghost)
                    {
                        return j;
                    }
                }
            }

            if (userPokemon.type == PokemonTypes.Dragon)
            {
                for (int i = 0; i < enemyTeam.Count; i++)
                {
                    if (enemyTeam[i].type == PokemonTypes.Ice || enemyTeam[i].type == PokemonTypes.Dragon)
                    {
                        return i;
                    }
                }
            }

            if (userPokemon.type == PokemonTypes.Ground)
            {
                for (int i = 0; i < enemyTeam.Count; i++)
                {
                    if (enemyTeam[i].type == PokemonTypes.Water || enemyTeam[i].type == PokemonTypes.Ice || enemyTeam[i].type == PokemonTypes.Grass)
                    {
                        return i;
                    }
                }

                for (int j = 0; j < enemyTeam.Count; j++)
                {
                    if (enemyTeam[j].type == PokemonTypes.Bug)
                    {
                        return j;
                    }
                }
            }

            if (userPokemon.type == PokemonTypes.Fighting)
            {
                for (int i = 0; i < enemyTeam.Count; i++)
                {
                    if (enemyTeam[i].type == PokemonTypes.Flying || enemyTeam[i].type == PokemonTypes.Psychic || enemyTeam[i].type == PokemonTypes.Ghost)
                    {
                        return i;
                    }
                }

                for (int j = 0; j < enemyTeam.Count; j++)
                {
                    if (enemyTeam[j].type == PokemonTypes.Poison || enemyTeam[j].type == PokemonTypes.Bug)
                    {
                        return j;
                    }
                }
            }

            if (userPokemon.type == PokemonTypes.Bug)
            {
                for (int i = 0; i < enemyTeam.Count; i++)
                {
                    if (enemyTeam[i].type == PokemonTypes.Fire || enemyTeam[i].type == PokemonTypes.Flying || enemyTeam[i].type == PokemonTypes.Rock)
                    {
                        return i;
                    }
                }

                for (int j = 0; j < enemyTeam.Count; j++)
                {
                    if (enemyTeam[j].type == PokemonTypes.Fighting || enemyTeam[j].type == PokemonTypes.Poison || enemyTeam[j].type == PokemonTypes.Ghost)
                    {
                        return j;
                    }
                }
            }

            if (userPokemon.type == PokemonTypes.Flying)
            {
                for (int i = 0; i < enemyTeam.Count; i++)
                {
                    if (enemyTeam[i].type == PokemonTypes.Electric || enemyTeam[i].type == PokemonTypes.Ice || enemyTeam[i].type == PokemonTypes.Rock)
                    {
                        return i;
                    }
                }
            }

            if (userPokemon.type == PokemonTypes.Ghost)
            {
                for (int i = 0; i < enemyTeam.Count; i++)
                {
                    if (enemyTeam[i].type == PokemonTypes.Ghost)
                    {
                        return i;
                    }
                }
            }

            return 1;
        }

        public static double Effectiveness(Moves move, Pokemon target)
        {
            if (target.type == PokemonTypes.Fire)
            {
                if (move.type.ToString().Equals("Water") || move.type.ToString().Equals("Ground") || move.type.ToString().Equals("Rock"))
                {
                    return 2;
                }
                else if (move.type.ToString().Equals("Fire") || move.type.ToString().Equals("Grass") || move.type.ToString().Equals("Ice") || move.type.ToString().Equals("Bug") || move.type.ToString().Equals("Steel"))
                {
                    return 0.5;
                }
            }

            if (target.type == PokemonTypes.Water)
            {
                if (move.type.ToString().Equals("Electric") || move.type.ToString().Equals("Grass"))
                {
                    return 2;
                }
                else if (move.type.ToString().Equals("Fire") || move.type.ToString().Equals("Water") || move.type.ToString().Equals("Ice") || move.type.ToString().Equals("Steel"))
                    return 0.5;
            }

            if (target.type == PokemonTypes.Grass)
            {
                if (move.type.ToString().Equals("Fire") || move.type.ToString().Equals("Ice") || move.type.ToString().Equals("Poison") || move.type.ToString().Equals("Flying") || move.type.ToString().Equals("Bug"))
                {
                    return 2;
                }
                else if (move.type.ToString().Equals("Water") || move.type.ToString().Equals("Electric") || move.type.ToString().Equals("Grass") || move.type.ToString().Equals("Ground"))
                {
                    return 0.5;
                }
            }

            if (target.type == PokemonTypes.Normal)
            {
                if (move.type.ToString().Equals("Fighting"))
                {
                    return 2;
                }
            }

            if (target.type == PokemonTypes.Rock)
            {
                if (move.type.ToString().Equals("Water") || move.type.ToString().Equals("Grass") || move.type.ToString().Equals("Fighting") || move.type.ToString().Equals("Ground") || move.type.ToString().Equals("Steel"))
                {
                    return 2;
                }
                else if (move.type.ToString().Equals("Water") || move.type.ToString().Equals("Electric") || move.type.ToString().Equals("Grass") || move.type.ToString().Equals("Ground"))
                {
                    return 0.5;
                }
            }

            if (target.type == PokemonTypes.Electric)
            {
                if (move.type.ToString().Equals("Ground"))
                {
                    return 2;
                }
                else if (move.type.ToString().Equals("Electric") || move.type.ToString().Equals("Flying") || move.type.ToString().Equals("Steel"))
                {
                    return 0.5;
                }
            }

            if (target.type == PokemonTypes.Poison)
            {
                if (move.type.ToString().Equals("Psychic") || move.type.ToString().Equals("Ground"))
                {
                    return 2;
                }
                else if (move.type.ToString().Equals("Fighting") || move.type.ToString().Equals("Grass") || move.type.ToString().Equals("Poison") || move.type.ToString().Equals("Bug"))
                {
                    return 0.5;
                }
            }

            if (target.type == PokemonTypes.Dragon)
            {
                if (move.type.ToString().Equals("Ice") || move.type.ToString().Equals("Dragon"))
                {
                    return 2;
                }
                else if (move.type.ToString().Equals("Fire") || move.type.ToString().Equals("Grass") || move.type.ToString().Equals("Water") || move.type.ToString().Equals("Electric"))
                {
                    return 0.5;
                }
            }

            if (target.type == PokemonTypes.Ground)
            {
                if (move.type.ToString().Equals("Ice") || move.type.ToString().Equals("Dragon"))
                {
                    return 2;
                }
                else if (move.type.ToString().Equals("Fire") || move.type.ToString().Equals("Grass") || move.type.ToString().Equals("Water") || move.type.ToString().Equals("Electric"))
                {
                    return 0.5;
                }
            }

            if (target.type == PokemonTypes.Fighting)
            {
                if (move.type.ToString().Equals("Flying") || move.type.ToString().Equals("Psychic"))
                {
                    return 2;
                }
                else if (move.type.ToString().Equals("Bug") || move.type.ToString().Equals("Rock") || move.type.ToString().Equals("Dark"))
                {
                    return 0.5;
                }
            }

            if (target.type == PokemonTypes.Bug)
            {
                if (move.type.ToString().Equals("Fire") || move.type.ToString().Equals("Flying") || move.type.ToString().Equals("Rock"))
                {
                    return 2;
                }
                else if (move.type.ToString().Equals("Grass") || move.type.ToString().Equals("Fighting") || move.type.ToString().Equals("Ground"))
                {
                    return 0.5;
                }
            }

            if (target.type == PokemonTypes.Flying)
            {
                if (move.type.ToString().Equals("Ice") || move.type.ToString().Equals("Electric") || move.type.ToString().Equals("Rock"))
                {
                    return 2;
                }
                else if (move.type.ToString().Equals("Fighting") || move.type.ToString().Equals("Grass") || move.type.ToString().Equals("Bug"))
                {
                    return 0.5;
                }
                else if (move.type.ToString().Equals("Ground"))
                {
                    return 0;
                }
            }

            if (target.type == PokemonTypes.Ghost)
            {
                if (move.type.ToString().Equals("Ghost") || move.type.ToString().Equals("Dark"))
                {
                    return 2;
                }
                else if (move.type.ToString().Equals("Poison") || move.type.ToString().Equals("Bug"))
                {
                    return 0.5;
                }
                else if (move.type.ToString().Equals("Normal") || move.type.ToString().Equals("Fighting"))
                {
                    return 0;
                }
            }

            return 1;
        }
    }
}
