﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Battle.Battle
{
    [Serializable]
    public class State
    {
        public List<Pokemon> yourPokemons { get; }
        public List<Pokemon> opponentPokemons { get; }
        public Pokemon currentPokemon { get; }
        public Pokemon oppoentCurrentPokemon { get; }
        public int timer { get; }

        public State(List<Pokemon> poke, List<Pokemon> oPoke, Pokemon current, Pokemon oCurrent, int timer)
        {
            yourPokemons = poke;
            opponentPokemons = oPoke;
            currentPokemon = current;
            oppoentCurrentPokemon = oCurrent;
            this.timer = timer;
        }
    }
}
